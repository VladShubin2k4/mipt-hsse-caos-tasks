#include <errno.h>  // for debug
#include <fcntl.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
//#include <iostream>  // for debug
#include <regex>
#include <string_view>

#include "../../libsources/iostream.hpp"

using namespace stdlike;

class InputTest : public testing::Test {
 protected:
  InputTest() : stdin_d(dup(STDIN_FILENO)) {
    int fd = open(kInputFilePath.data(), O_RDONLY);
    if (dup2(fd, STDIN_FILENO) == -1) {
      close(fd);
      throw std::runtime_error("couldn't open input-source file");
    }
    close(fd);
  }

  ~InputTest() {
    dup2(stdin_d, STDIN_FILENO);
    close(stdin_d);
  }

 private:
  static constexpr std::string_view kInputFilePath =
      "./input_output_files/input.txt";
  int stdin_d{-1};
};

TEST_F(InputTest, FundamentalTypes) {
  char ch_num, ch_classic;
  cin >> ch_num >> ch_classic;
  ASSERT_EQ(ch_num, '1');
  ASSERT_EQ(ch_classic, 'c');

  int num;
  long long big_num;
  double floating_num;

  cin >> num >> big_num >> floating_num;
  ASSERT_EQ(num, 32);
  ASSERT_EQ(big_num, 8589934588);
  ASSERT_DOUBLE_EQ(floating_num, 3.1415926535);

  cin >> num >> big_num >> floating_num;
  ASSERT_EQ(num, -32);
  ASSERT_EQ(big_num, -8589934588);
  ASSERT_DOUBLE_EQ(floating_num, -3.1415926535);

  bool boolean(false);
  cin >> boolean;
  ASSERT_TRUE(boolean);
  cin >> boolean;
  ASSERT_FALSE(boolean);
}

TEST_F(InputTest, BoundaryValues) {
  {
    int positive_num, negative_num;

    cin >> positive_num >> negative_num;
    ASSERT_EQ(positive_num, std::numeric_limits<int>::max());
    ASSERT_EQ(negative_num, std::numeric_limits<int>::min());
  }

  {
    long long positive_num, negative_num;

    cin >> positive_num >> negative_num;
    ASSERT_EQ(positive_num, std::numeric_limits<long long>::max());
    ASSERT_EQ(negative_num, std::numeric_limits<long long>::min());
  }
}

TEST_F(InputTest, LeadingZeroes) {
  int num;

  cin >> num;
  ASSERT_EQ(num, 101);

  cin >> num;
  ASSERT_EQ(num, -101);
}

TEST_F(InputTest, Unsigned) {
  unsigned int num;
  cin >> num;
  ASSERT_EQ(num, std::numeric_limits<unsigned int>::max());

  cin >> num;
  ASSERT_EQ(num, 2147483648);
}

TEST_F(InputTest, FloatingComplexInput) {
  double a, b;
  
  cin >> a >> b;

  ASSERT_DOUBLE_EQ(a, 10.0);
  ASSERT_DOUBLE_EQ(b, 0.01);
}

TEST_F(InputTest, InputVectorWithWrongInput) {
  std::vector<int> v(8);
  for (auto& elem : v) {
    cin >> elem;
  }

  std::vector<int> expected_v = {1, 0, -1, 0, 0, 0, 0, 0};
  
  ASSERT_TRUE(v == expected_v);
}

class OutputTest : public testing::Test {
 protected:
  OutputTest() : stdout_d(dup(STDOUT_FILENO)), buf(32, '\0') {
    int fd = open(kOutputFilePath.data(), O_RDWR | O_CREAT | O_TRUNC,
                  S_IRUSR | S_IWUSR);
    if (dup2(fd, STDOUT_FILENO) == -1) {
      close(fd);
      throw std::runtime_error("couldn't open file for output");
    }
    fin.open(kOutputFilePath.data());
    close(fd);
  }

  char* ExportOutput() {
    fin.getline(buf.data(), buf.size(), '\n');
    return buf.data();
  }

  void RestoreIfstream() {
    int pos = fin.tellg();
    if (pos == -1) {
      if (fin.is_open()) {
        fin.close();
      }
      fin.open(kOutputFilePath.data());
    }
  }

  ~OutputTest() {
    dup2(stdout_d, STDOUT_FILENO);
    close(stdout_d);
  }

 private:
  static constexpr std::string_view kOutputFilePath =
      "./input_output_files/output.txt";
  int stdout_d{-1};

  std::ifstream fin;
  std::string buf;
};

TEST_F(OutputTest, FundamentalTypes) {
  constexpr char ch_num('1'), ch_classic('c');
  cout << ch_num << ' ' << ch_classic << '\n';
  cout.flush();
  ASSERT_STREQ("1 c", ExportOutput());

  constexpr int num(32);
  constexpr long long big_num(8589934588LL);
  cout << num << ' ' << big_num << '\n';
  cout.flush();
  ASSERT_STREQ("32 8589934588", ExportOutput());

  constexpr double floating_num(3.14159);
  constexpr double floating_expected(3.14159);
  cout << floating_num << '\n';
  cout.flush();
  ASSERT_NEAR(floating_expected, std::stod(ExportOutput()), 0.00001);

  cout << false << ' ' << true << '\n';
  cout.flush();
  ASSERT_STREQ("0 1", ExportOutput());
}

TEST_F(OutputTest, Pointers) {
  const char* char_ptr = "Should be string!";
  cout << char_ptr << '\n';
  cout.flush();
  ASSERT_STREQ(char_ptr, ExportOutput());

  void* ptr = &cout;
  cout << ptr << '\n';
  cout.flush();
  ASSERT_THAT(ExportOutput(), ::testing::ContainsRegex("0x[0-9a-f]+"));
}

TEST_F(OutputTest, FlushingOutputBeforeInput) {
  cout.flush();
  static constexpr std::string_view output_before_input = "Some Output!";
  cout << output_before_input.data() << '\n';
  ASSERT_STREQ("\0", ExportOutput());

  bool trigger;
  cin >> trigger;
  RestoreIfstream();
  ASSERT_STREQ("Some Output!", ExportOutput());
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}