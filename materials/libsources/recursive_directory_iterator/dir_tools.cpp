#include "rec_dir_it.hpp"

namespace stdlike {

recursive_directory_iterator begin(recursive_directory_iterator it) noexcept {
  return it;
}

recursive_directory_iterator end(recursive_directory_iterator) noexcept {
  return {};
}

Path& Path::operator/=(const char* filename) {
  if (path_.back() != '/') {
    path_.push_back('/');
  }
  path_ += filename;
  return *this;
}

ssize_t Path::FindLastSlash() {
  for (auto i = std::ssize(path_) - 1; i >= 0; --i) {
    if (path_[i] == '/') {
      return i;
    }
  }
  return -1;
}

class NonDirectoryError : public std::exception {
 public:
  [[nodiscard]] const char* what() const noexcept override {
    return "filesystem error: cannot read dir: file is not a directory";
  }
};

class PermissionDenied : public std::exception {
 public:
  [[nodiscard]] const char* what() const noexcept override {
    return "filesystem error: permission denied";
  }
};

directory_options operator|(directory_options lhs, directory_options rhs) {
  using type = std::underlying_type_t<directory_options>;
  return static_cast<directory_options>(static_cast<type>(lhs) |
                                        static_cast<type>(rhs));
}

directory_options operator&(directory_options lhs, directory_options rhs) {
  using type = std::underlying_type_t<directory_options>;
  return static_cast<directory_options>(static_cast<type>(lhs) &
                                        static_cast<type>(rhs));
}

directory_entry::directory_entry(const char* p) : pth_(p) {
  DIR* dir = opendir(p);
  if (dir != nullptr) {
    const auto* p_dirent = readdir(dir);
    while (p_dirent != nullptr && p_dirent->d_name[0] == '.') {
      p_dirent = readdir(dir);
    }
    if (p_dirent != nullptr) {
      lstat(p, &stat_);
    }
    closedir(dir);
  } else if (errno == ENOTDIR) {
    throw NonDirectoryError();
  } else if (errno == EACCES) {
    throw PermissionDenied();
  }
}

recursive_directory_iterator::recursive_directory_iterator(
    const char* p, directory_options options)
    : opts_(options), M_dirs_(new std::stack<DIR*>(), Deleter) {
  if (char* absolute_path = realpath(p, nullptr); absolute_path != nullptr) {
    ProcessOpts(opendir(absolute_path));
    ptr_ = std::make_shared<directory_entry>(absolute_path);
    UpdDirEntry(false);
    free(absolute_path);
  } else {
    throw std::invalid_argument("Invalid path!");
  }
}

void recursive_directory_iterator::ProcessOpts(DIR* dir) {
  if (dir != nullptr) {
    M_dirs_->emplace(dir);
  } else if (errno == ENOTDIR) {
    throw NonDirectoryError();
  } else if (errno == EACCES && !PermissionDeniedSkip()) {
    throw PermissionDenied();
  }
}

dirent* recursive_directory_iterator::pop() {
  if (depth() == 0) {
    *this = end(*this);
    return nullptr;
  }
  closedir(M_dirs_->top());
  M_dirs_->pop();

  stat(ptr_->path(), &ptr_->stat_);
  if (!ptr_->is_directory()) {
    ptr_->pth_.RemoveFilename();
  }
  
  return UpdDirEntry(true);
}

dirent* recursive_directory_iterator::UpdDirEntry(bool remove_file_from_path) {
  if (remove_file_from_path) {
    ptr_->pth_.RemoveFilename();
  }
  auto* p_dirent = readdir(M_dirs_->top());
  while (p_dirent != nullptr && p_dirent->d_name[0] == '.') {
    p_dirent = readdir(M_dirs_->top());
  }

  if (p_dirent != nullptr) {
    ptr_->pth_ /= p_dirent->d_name;
    lstat(ptr_->path(), &ptr_->stat_);
    if (ProcessDirectories()) {
      return UpdDirEntry(true);
    }
  } else {
    while (ptr_ != nullptr && p_dirent == nullptr) {
      if (depth() >= 0) {
        p_dirent = pop();
      }
    }
  }
  return p_dirent;
}

recursive_directory_iterator& recursive_directory_iterator::operator++() {
  if (ptr_ == nullptr) {
    *this = end(*this);
    return *this;
  }
  if (ptr_->is_symlink()) [[unlikely]] {
    UpdDirEntry(!SymLinksCheck());
  } else [[likely]] {
    UpdDirEntry(!ptr_->is_directory());
  }
  return *this;
}

bool recursive_directory_iterator::ProcessDirectories() {
  DIR* dir(nullptr);
  if (SymLinksCheck() && ptr_->is_symlink()) {
    char* norm_path = realpath(ptr_->path(), nullptr);
    stat(norm_path, &ptr_->stat_);
    if (ptr_->is_directory()) {
      dir = opendir(ptr_->path());
      try {
        ProcessOpts(dir);
      } catch (...) {
        free(norm_path);
        throw;
      }
    }
    lstat(ptr_->path(), &ptr_->stat_);
    free(norm_path);
    return dir == nullptr;
  }
  if (ptr_->is_directory()) {
    dir = opendir(ptr_->path());
    ProcessOpts(dir);
    return dir == nullptr;
  }
  return false;
}

bool recursive_directory_iterator::SymLinksCheck() {
  return (opts_ & directory_options::follow_directory_symlink) ==
         directory_options::follow_directory_symlink;
}

bool recursive_directory_iterator::PermissionDeniedSkip() {
  return (opts_ & directory_options::skip_permission_denied) ==
         directory_options::skip_permission_denied;
}

void recursive_directory_iterator::Deleter(std::stack<DIR*>* dir_stack) {
  while (!dir_stack->empty()) {
    closedir(dir_stack->top());
    dir_stack->pop();
  }
  delete dir_stack;
}

}  // namespace stdlike
