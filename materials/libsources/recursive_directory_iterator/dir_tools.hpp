#pragma once

#include <dirent.h>
#include <sys/stat.h>

#include <cerrno>
#include <chrono>
#include <cstddef>
#include <cstdlib>
#include <memory>
#include <stack>

namespace stdlike {

enum class directory_options : char {
  none = 0,
  follow_directory_symlink = 1,
  skip_permission_denied = 2,
};

directory_options operator|(directory_options lhs, directory_options rhs);
directory_options operator&(directory_options lhs, directory_options rhs);

enum class perms : mode_t {
  none = 0,
  owner_read = S_IRUSR,
  owner_write = S_IWUSR,
  owner_exec = S_IXUSR,
  owner_all = S_IRWXU,
  group_read = S_IRGRP,
  group_write = S_IWGRP,
  group_exec = S_IXGRP,
  group_all = S_IRWXG,
  others_read = S_IROTH,
  others_write = S_IWOTH,
  others_exec = S_IXOTH,
  others_all = S_IRWXO,
  all = owner_all | group_all | others_all,
  set_uid = S_ISUID,
  set_gid = S_ISGID,
  sticky_bit = S_ISVTX,
  mask = all | set_uid | set_gid | sticky_bit,
  unknown = 0xFFFF,
};

using file_time_type = std::chrono::time_point<std::chrono::_V2::system_clock>;
using permitions_type = perms;

class Path {
 public:
  Path(const char* source) : path_(source) {}
  [[nodiscard]] const char* path() const noexcept { return path_.c_str(); }
  Path& operator/=(const char*);
  Path& RemoveFilename() {
    path_.resize(FindLastSlash());
    return *this;
  }

 private:
  std::pmr::string path_;

  ssize_t FindLastSlash();
};

class directory_entry {
 public:
  directory_entry(const directory_entry&) = default;
  directory_entry(directory_entry&&) noexcept = default;

  explicit directory_entry(const char* p);

  directory_entry& operator=(const directory_entry&) = default;
  directory_entry& operator=(directory_entry&&) noexcept = default;

  [[nodiscard]] const char* path() const noexcept { return pth_.path(); }

  bool is_block_file() const { return S_ISBLK(stat_.st_mode); }
  bool is_character_file() const { return S_ISCHR(stat_.st_mode); }
  bool is_directory() const { return S_ISDIR(stat_.st_mode); }
  bool is_fifo() const { return S_ISFIFO(stat_.st_mode); }
  bool is_regular_file() const { return S_ISREG(stat_.st_mode); }
  bool is_socket() const { return S_ISSOCK(stat_.st_mode); }
  bool is_symlink() const { return S_ISLNK(stat_.st_mode); }

  std::size_t file_size() const { return stat_.st_size; }
  std::size_t hard_link_count() const { return stat_.st_nlink; }

  file_time_type last_write_time() const {
    return std::chrono::system_clock::from_time_t(stat_.st_ctime);
  }

  permitions_type permitions() const {
    return static_cast<permitions_type>(stat_.st_mode);
  }

 private:
  struct stat stat_;
  Path pth_;
  friend class recursive_directory_iterator;

  long FindLastSlash();
};

class recursive_directory_iterator {
 public:
  using value_type = directory_entry;
  using difference_type = ptrdiff_t;
  using pointer = std::shared_ptr<value_type>;
  using reference = value_type&;

  recursive_directory_iterator() = default;
  explicit recursive_directory_iterator(
      const char* p, directory_options options = directory_options::none);

  recursive_directory_iterator& operator++();
  bool operator==(const recursive_directory_iterator& other) const = default;

  reference operator*() { return *ptr_; }
  pointer operator->() const { return ptr_; }

  int depth() const { return static_cast<int>(std::ssize(*M_dirs_) - 1); }
  dirent* pop();

  static void Deleter(std::stack<DIR*>* dir_stack);

 private:
  pointer ptr_{nullptr};

  directory_options opts_{directory_options::none};
  std::shared_ptr<std::stack<DIR*>> M_dirs_{nullptr};

  dirent* UpdDirEntry(bool remove_file_from_path);
  bool ProcessDirectories();
  void ProcessOpts(DIR* dir);

  bool SymLinksCheck();
  bool PermissionDeniedSkip();
};

recursive_directory_iterator begin(recursive_directory_iterator it) noexcept;
recursive_directory_iterator end(recursive_directory_iterator) noexcept;

}  // namespace stdlike
