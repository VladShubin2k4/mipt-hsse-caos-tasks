#pragma once

#include <unistd.h>

#include <bit>
#include <cmath>
#include <cctype>
#include <concepts>
#include <limits>
#include <type_traits>

namespace stdlike {

std::size_t strlen(const char* start);

constexpr void reverse(char* left_border, char* right_border) {
  if (left_border < right_border) {
    for (--right_border; left_border < right_border;
         ++left_border, --right_border) {
      char tmp = *left_border;
      *left_border = *right_border;
      *right_border = tmp;
    }
  }
}

class basic_streambuf {
 public:
  static constexpr int eof = -1;

  basic_streambuf() = default;
  using int_type = long;
  using streamsize = long long;

  // Get area:
  streamsize in_avail();
  int_type snextc();
  int_type sbumpc();
  int_type sgetc();
  void sgetn(char* s, streamsize n);
  // Putback:
  int_type sputbackc(char ch);
  int_type sungetc();
  // Put area:
  void sputc(char ch);
  void sputn(const char* s, streamsize n);

  // get area
  int_type underflow();
  int_type uflow();

  // put area
  void overflow(char ch = eof);

  // putback
  [[nodiscard]] int_type pbackfail(int_type c = eof) const { return c; }

 protected:
  //  basic_streambuf(const basic_streambuf& rhs);
  //  basic_streambuf& operator=(const basic_streambuf& rhs);
  //  void swap(basic_streambuf& rhs);

  // Get area:
  char* eback() { return get_begin; }
  char* gptr() { return get_next; }
  char* egptr() { return get_end; }
  void gbump(int n) { get_next += n; }
  void setg(char* gbeg, char* gnext, char* gend);

  // Put area:
  char* pbase() { return put_begin; }
  char* pptr() { return put_next; }
  char* epptr() { return put_end; }
  void pbump(int n) { put_next += n; }
  void setp(char* pbeg, char* pend);

  static const streamsize buf_size = 1024;
  char get_area[buf_size]{};

  char* get_begin{nullptr};
  char* get_next{nullptr};
  char* get_end{nullptr};

  char put_area[buf_size]{};
  char* put_begin{put_area};
  char* put_next{put_area};
  char* put_end{put_area + buf_size};
};

class basic_ostream;

class basic_istream {
  enum class NumType {
    Integer,
    Float,
  };

 public:
  using int_type = long;

  basic_istream() = default;
  explicit basic_istream(basic_streambuf* sb) : sb_(sb) {}
  ~basic_istream();

  basic_istream& operator>>(bool& n);
  basic_istream& operator>>(char& n);
  basic_istream& operator>>(short& n);
  basic_istream& operator>>(unsigned short& n);
  basic_istream& operator>>(int& n);
  basic_istream& operator>>(unsigned int& n);
  basic_istream& operator>>(long& n);
  basic_istream& operator>>(unsigned long& n);
  basic_istream& operator>>(long long& n);
  basic_istream& operator>>(unsigned long long& n);
  basic_istream& operator>>(float& f);
  basic_istream& operator>>(double& f);
  basic_istream& operator>>(long double& f);

  int_type get();
  int_type peek();

  void tie(basic_ostream* os);

  basic_streambuf* rdbuf() { return sb_; }
  void setbuf(basic_streambuf* buf) { sb_ = buf; }
  [[maybe_unused]] bool good() { return rdbuf() != nullptr; }

 protected:
  basic_istream(const basic_istream&) = delete;
  //  basic_istream(basic_istream &&rhs); TODO

  basic_istream& operator=(const basic_istream&) = delete;
  //  basic_istream &operator=(basic_istream &&rhs); TODO
  //  void swap(basic_istream &rhs); TODO

 private:
  basic_streambuf* sb_{nullptr};
  basic_ostream* os_{nullptr};
  static const int buf_sz = 64;
  char buf_[buf_sz];

  template <std::integral T>
  static T SeqToInt(const char* buf, int sz, T radix = 10);
  int ReadNum(char* buf, int sz, NumType type);

  template <std::floating_point T>
  static T SeqToFloat(const char* buf, int sz, T radix = 10);
};


class basic_ostream {
 public:
  basic_ostream() = default;
  explicit basic_ostream(basic_streambuf* sb) : sb_(sb) {}
  ~basic_ostream();

  basic_ostream& operator<<(bool n);
  basic_ostream& operator<<(char c);
  basic_ostream& operator<<(const char* s);
  basic_ostream& operator<<(short n);
  basic_ostream& operator<<(unsigned short n);
  basic_ostream& operator<<(int n);
  basic_ostream& operator<<(unsigned int n);
  basic_ostream& operator<<(long n);
  basic_ostream& operator<<(unsigned long n);
  basic_ostream& operator<<(long long n);
  basic_ostream& operator<<(unsigned long long n);
  basic_ostream& operator<<(float f);
  basic_ostream& operator<<(double f);
  basic_ostream& operator<<(long double f);

  basic_ostream& operator<<(const void* ptr);

  basic_streambuf* rdbuf() { return sb_; }
  void setbuf(basic_streambuf* buf) { sb_ = buf; }
  [[maybe_unused]] bool good() { return rdbuf() != nullptr; }

  void put(char ch);
  void tie(basic_istream* in);
  void flush() { sb_->overflow(); }

 protected:
  basic_ostream(const basic_ostream&) = delete;
  //  basic_ostream(basic_ostream &&rhs); TODO

  basic_ostream& operator=(const basic_ostream&) = delete;
  //  basic_ostream &operator=(basic_ostream &&rhs); TODO
  //  void swap(basic_ostream &rhs); TODO

  template <std::integral T>
  int ConvertInt(T num, T radix = 10);

  template <std::floating_point T>
  int ConvertFloatingPointType(T num, long long precision = 6,
                               long long radix = 10);

 private:
  basic_streambuf* sb_{nullptr};
  basic_istream* in_{nullptr};

  static const int buf_sz = 64;
  char buf_[buf_sz];
};

using istream = basic_istream;
using ostream = basic_ostream;

extern istream cin;
extern ostream cout;

class ios_base {
 public:
  ios_base();
};

extern ios_base ios;
}  // namespace stdlike
