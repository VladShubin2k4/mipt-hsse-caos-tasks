#include "iostream.hpp"

namespace stdlike {

std::size_t strlen(const char* start) {
  const char* end = start;
  while (*end != '\0') {
    ++end;
  }
  return end - start;
}

basic_streambuf std_io_buf;

istream cin(&std_io_buf);
ostream cout(&std_io_buf);

ios_base ios;

basic_streambuf::streamsize basic_streambuf::in_avail() {
  if ((eback() == nullptr && egptr() == nullptr) || gptr() == egptr()) {
    return -1;
  }
  return egptr() - gptr();
}
basic_streambuf::int_type basic_streambuf::snextc() {
  if (sbumpc() != eof) {
    return sgetc();
  }
  return eof;
}
basic_streambuf::int_type basic_streambuf::sbumpc() {
  if (gptr() == nullptr || gptr() == egptr()) {
    return uflow();
  }
  gbump(1);
  return static_cast<int_type>(gptr()[-1]);
}
basic_streambuf::int_type basic_streambuf::sgetc() {
  if (gptr() == nullptr || gptr() == egptr()) {
    return underflow();
  }
  return static_cast<int_type>(*gptr());
}
void basic_streambuf::sgetn(char* s, basic_streambuf::streamsize n) {
  char ch;
  for (int i = 0; i < n; ++i) {
    if (ch = static_cast<char>(sbumpc()); ch == eof) {
      ch = static_cast<char>(underflow());
    }
    s[i] = ch;
  }
}
basic_streambuf::int_type basic_streambuf::sputbackc(char ch) {
  if (gptr() == nullptr || (gptr() == eback()) || (ch != gptr()[-1])) {
    return pbackfail(ch);
  }
  gbump(-1);
  return static_cast<int_type>(*gptr());
}

basic_streambuf::int_type basic_streambuf::sungetc() {
  if (gptr() == nullptr || gptr() == eback()) {
    return pbackfail();
  }
  gbump(-1);
  return static_cast<int_type>(*gptr());
}

void basic_streambuf::setg(char* gbeg, char* gnext, char* gend) {
  get_begin = gbeg;
  get_next = gnext;
  get_end = gend;
}

basic_streambuf::int_type basic_streambuf::underflow() {
  auto readed = read(STDIN_FILENO, get_area, buf_size);
  setg(get_area, get_area, get_area + readed);
  return static_cast<int_type>(*gptr());
}

basic_streambuf::int_type basic_streambuf::uflow() {
  if (underflow() == eof) {
    return eof;
  }
  gbump(1);
  return gptr()[-1];
}

void basic_streambuf::setp(char* pbeg, char* pend) {
  put_begin = pbeg;
  put_next = put_begin;
  put_end = pend;
}

// istream
basic_istream::int_type basic_istream::get() {
  if (good()) {
    return rdbuf()->sbumpc();
  }
  return basic_streambuf::eof;
}
basic_istream::int_type basic_istream::peek() {
  if (good()) {
    return rdbuf()->sgetc();
  }
  return basic_streambuf::eof;
}

basic_istream& basic_istream::operator>>(bool& n) {
  os_->flush();
  auto ch = static_cast<char>(get());
  while (std::isspace(ch) != 0 || std::iscntrl(ch) != 0) {
    ch = static_cast<char>(get());
  }
  n = (std::isdigit(ch) != 0 && ch != '0');
  return *this;
}
basic_istream& basic_istream::operator>>(char& ch) {
  os_->flush();
  auto input_ch = static_cast<char>(get());
  while (std::isspace(input_ch) != 0 || std::iscntrl(input_ch) != 0) {
    input_ch = static_cast<char>(get());
  }
  ch = input_ch;
  return *this;
}

int basic_istream::ReadNum(char* buf, int sz, basic_istream::NumType type) {
  int pos = 0;
  auto c_i = static_cast<char>(peek());
  while (std::isspace(c_i) != 0 || std::iscntrl(c_i) != 0) {
    c_i = static_cast<char>(rdbuf()->snextc());
  }
  bool dot_found = c_i == '.';
  bool sign_found = c_i == '-';
  if (c_i == '-') {
    buf[pos++] = c_i;
    sz++;
  } else if (std::isdigit(c_i) == 0 && (type == NumType::Float && !dot_found)) {
    if (std::isalpha(c_i) != 0 || std::isdigit(peek()) == 0) {
      return 0;
    }
    c_i = rdbuf()->snextc();
    dot_found = c_i == '.';
  }
  while (c_i == '0') {
    c_i = static_cast<char>(rdbuf()->snextc());
  }
  
  dot_found = c_i == '.';
  if (dot_found && type == NumType::Integer) {
    return pos;
  }

  buf[pos] = c_i;
  pos += int(std::isdigit(c_i) || c_i == '.' || (c_i == '-' && !sign_found));
  
  for (c_i = static_cast<char>(sb_->snextc());
       pos < sz && (std::isdigit(c_i) || c_i == '.');
       c_i = static_cast<char>(sb_->snextc())) {
    if ((std::isdigit(c_i) != 0 ||
         (c_i == '.' && type == NumType::Float && !dot_found)) &&
        pos < buf_sz) {
      dot_found = (c_i == '.');
      buf[pos++] = c_i;
    } else if (pos > 0) {
      break;
    }
  }
  return pos;
}

template <std::integral T>
T basic_istream::SeqToInt(const char* buf, int sz, T radix) {
  T res = 0;
  T power = 1;

  int border = buf[0] == '-' ? 1 : 0;
  T sign = buf[0] == '-' ? -1 : 1;
  const T kLimitValue = buf[0] == '-' ? std::numeric_limits<T>::min()
                                      : std::numeric_limits<T>::max();

  for (int i = sz - 1; i >= border; --i, power *= radix) {
    if (res > std::numeric_limits<T>::max() - T(buf[i] - '0') * power) {
      return kLimitValue;
    }
    res += (buf[i] - '0') * power;
  }
  return res * sign;
}

template <typename T>
const int max_len_of_int =
    std::trunc(std::log10(std::numeric_limits<T>::max())) + 1;

basic_istream& basic_istream::operator>>(short& n) {
  os_->flush();
  n = SeqToInt<short>(buf_,
                      ReadNum(buf_, max_len_of_int<short>, NumType::Integer));
  return *this;
}
basic_istream& basic_istream::operator>>(unsigned short& n) {
  os_->flush();
  n = SeqToInt<unsigned short>(
      buf_, ReadNum(buf_, max_len_of_int<unsigned short>, NumType::Integer));
  return *this;
}
basic_istream& basic_istream::operator>>(int& n) {
  os_->flush();
  n = SeqToInt<int>(buf_, ReadNum(buf_, max_len_of_int<int>, NumType::Integer));
  return *this;
}
basic_istream& basic_istream::operator>>(unsigned int& n) {
  os_->flush();
  n = SeqToInt<unsigned int>(
      buf_, ReadNum(buf_, max_len_of_int<unsigned int>, NumType::Integer));
  return *this;
}
basic_istream& basic_istream::operator>>(long& n) {
  os_->flush();
  n = SeqToInt<long>(buf_,
                     ReadNum(buf_, max_len_of_int<long>, NumType::Integer));
  return *this;
}
basic_istream& basic_istream::operator>>(unsigned long& n) {
  os_->flush();
  n = SeqToInt<unsigned long>(
      buf_, ReadNum(buf_, max_len_of_int<unsigned long>, NumType::Integer));
  return *this;
}
basic_istream& basic_istream::operator>>(long long& n) {
  os_->flush();
  n = SeqToInt<long long>(
      buf_, ReadNum(buf_, max_len_of_int<long long>, NumType::Integer));
  return *this;
}
basic_istream& basic_istream::operator>>(unsigned long long& n) {
  os_->flush();
  n = SeqToInt<unsigned long long>(
      buf_,
      ReadNum(buf_, max_len_of_int<unsigned long long>, NumType::Integer));
  return *this;
}

template <std::floating_point T>
T basic_istream::SeqToFloat(const char* buf, int sz, T radix) {
  T sign = buf[0] == '-' ? -1.0 : 1.0;
  int dot_ptr = 0;
  while (dot_ptr < sz && buf[dot_ptr] != '.') {
    ++dot_ptr;
  }
  T res = SeqToInt(buf + int(buf[0] == '-'), dot_ptr - int(buf[0] == '-'),
                   static_cast<long long>(radix));
  if (dot_ptr == sz) {
    return sign * res;
  }
  int ten_power = sz - dot_ptr - 1;
  T floating_part =
      SeqToInt(buf + dot_ptr + 1, ten_power, static_cast<long long>(radix));
  for (int i = 0; i < ten_power; ++i) {
    floating_part /= radix;
  }
  res += floating_part;
  return sign * res;
}

basic_istream& basic_istream::operator>>(float& n) {
  os_->flush();
  n = SeqToFloat<float>(buf_, ReadNum(buf_, buf_sz, NumType::Float));
  return *this;
}
basic_istream& basic_istream::operator>>(double& n) {
  os_->flush();
  n = SeqToFloat<double>(buf_, ReadNum(buf_, buf_sz, NumType::Float));
  return *this;
}
basic_istream& basic_istream::operator>>(long double& n) {
  os_->flush();
  n = SeqToFloat<long double>(buf_, ReadNum(buf_, buf_sz, NumType::Float));
  return *this;
}

basic_istream::~basic_istream() { sb_->overflow(); }

void basic_istream::tie(basic_ostream* os) { os_ = os; }

// ostream
void basic_streambuf::overflow(char ch) {
  for (long i = pptr() - pbase(); i > 0;) {
    auto written_bytes =
        write(STDOUT_FILENO, put_area + (pptr() - pbase()) - i, i);
    if (written_bytes == -1) {
      throw "write failed";
    } else {
      i -= written_bytes;
    }
  }
  setp(put_area, put_area + buf_size);
  *pptr() = ch;
}
void basic_streambuf::sputc(char ch) {
  if (pptr() == epptr()) {
    overflow(ch);
  } else {
    *pptr() = ch;
    ++put_next;
  }
}
void basic_streambuf::sputn(const char* s, basic_streambuf::streamsize n) {
  for (int i = 0; i < n; ++i) {
    sputc(s[i]);
  }
}

void basic_ostream::put(char ch) { rdbuf()->sputc(ch); }

template <std::integral T>
int basic_ostream::ConvertInt(T num, T radix) {
  constexpr T dec = 10;
  int sz = 0;
  if constexpr (!std::is_unsigned_v<T>) {
    if (num < 0) {
      buf_[sz++] = '-';
    }
  }
  int begin = sz;
  do {
    auto digit = num % radix;
    digit = digit < 0 ? -digit : digit;

    buf_[sz++] = digit < dec ? static_cast<char>('0' + digit)
                             : static_cast<char>('a' + (digit - dec));
    num /= radix;
  } while (num != 0);
  reverse(buf_ + begin, buf_ + sz);
  buf_[sz] = '\0';
  return sz;
}

basic_ostream& basic_ostream::operator<<(bool n) {
  put(n ? '1' : '0');
  return *this;
}
basic_ostream& basic_ostream::operator<<(char ch) {
  put(ch);
  return *this;
}
basic_ostream& basic_ostream::operator<<(const char* str) {
  sb_->sputn(str, static_cast<basic_streambuf::streamsize>(strlen(str)));
  return *this;
}
basic_ostream& basic_ostream::operator<<(short n) {
  sb_->sputn(buf_, ConvertInt(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(unsigned short n) {
  sb_->sputn(buf_, ConvertInt(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(int n) {
  sb_->sputn(buf_, ConvertInt(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(unsigned int n) {
  sb_->sputn(buf_, ConvertInt(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(long n) {
  sb_->sputn(buf_, ConvertInt(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(unsigned long n) {
  sb_->sputn(buf_, ConvertInt(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(long long n) {
  sb_->sputn(buf_, ConvertInt(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(unsigned long long n) {
  sb_->sputn(buf_, ConvertInt(n));
  return *this;
}

template <std::floating_point T>
int basic_ostream::ConvertFloatingPointType(T num, long long precision,
                                            long long radix) {
  int idx = 0;
  if (num < 0) {
    buf_[idx++] = '-';
    num = -num;
  }

  auto integer_part = static_cast<long long>(num);
  auto fractional_part = num - static_cast<T>(integer_part);

  do {
    long long digit = integer_part % radix;
    buf_[idx++] = static_cast<char>('0' + digit);
    integer_part /= radix;
  } while (integer_part != 0);
  reverse(buf_ + int(buf_[0] == '-'), buf_ + idx);

  buf_[idx++] = '.';
  const T eps = 1e-6;

  for (int i = 0; fractional_part > eps && i < precision; ++i) {
    fractional_part *= static_cast<T>(radix);
    auto digit = static_cast<long long>(fractional_part);
    buf_[idx++] = static_cast<char>('0' + digit);
    fractional_part -= static_cast<T>(digit);
  }
  buf_[idx] = '\0';
  return idx;
}

basic_ostream& basic_ostream::operator<<(float n) {
  sb_->sputn(buf_, ConvertFloatingPointType(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(double n) {
  sb_->sputn(buf_, ConvertFloatingPointType(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(long double n) {
  sb_->sputn(buf_, ConvertFloatingPointType(n));
  return *this;
}
basic_ostream& basic_ostream::operator<<(const void* ptr) {
  const unsigned long long kHexRadix = 16ULL;
  const char hex_suf[2] = {'0', 'x'};

  auto address = std::bit_cast<unsigned long long>(ptr);
  auto sz = ConvertInt(address, kHexRadix);

  sb_->sputn(hex_suf, 2);
  sb_->sputn(buf_, sz);
  return *this;
}

basic_ostream::~basic_ostream() { sb_->overflow(); }
void basic_ostream::tie(basic_istream* in) { in_ = in; }

ios_base::ios_base() {
  cin.tie(&cout);
  cout.tie(&cin);
}
}  // namespace stdlike
